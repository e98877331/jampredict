#coding=utf-8

from django.shortcuts import render
from math import floor
import json
import csv
import time
import datetime
import pdb
import requests


def cyyadd(s, v):
    d = int(s)
    d = d + v
    if d < 0:
        d = 0
    return "%02d" % d


# Create your views here.
def index(request):
    return render(request, 'jampredict/index.html')

def dymap(request):
    dx = 40
    dy = 40
    with open("jampredict/data/SensorMap.json") as f:
        data = json.loads(f.read())


    with open("jampredict/data/viewinfo.json") as f:
        viewinfo = json.loads(f.read())

    with open("jampredict/data/map.json") as f:
        gridmap = f.read()
        gridmap = json.loads(gridmap)

    # with open("jampredict/data/data.csv") as f:
    #     csvdata = csv.reader(f)
    #     my_list = list(csvdata)
    #     my_dic = {}
    #     for row in my_list:
    #         my_dic[row[0]] = row[1:]

    # getting speed data from server
    querytime = None
    gettime = ''
    if 'time' in request.GET:
        gettime = request.GET['time']
        # 2015/05/22 21:52
        year = gettime[0:4]
        month = gettime[5:7]
        day = gettime[8:10]
        hour = gettime[11:13]
        minute = gettime[14:16]
        minute1 = cyyadd(minute, -2)
        minute2 = cyyadd(minute, 2)
        s1 = hour + ':' + minute1
        s2 = hour + ':' + minute2

        querytime = year + '-' + month + '-' + day + '/' + s1 + '/' + s2


    else:
        # querytime = '2015-05-21/00:00/00:06'
        querytime = ''

    if 'type' in request.GET:
        gettype = request.GET['type']
        if gettype == 'real':
            querytype = 'getRealSpeed/'
            querytype_ch = "實際車流"
        elif gettype == 'mlpredict':
            querytype = 'getPredictSpeed/'
            querytype_ch = 'ML車流預測'

        elif gettype == 'spredict':
            querytype = 'getStatisticSpeed/'
            querytype_ch = '統計車流預測'
    else:
        querytype = 'getRealSpeed/'
        querytype_ch = ""
        gettype = "real"

    if querytime == '':
        jdic = {'SectionData': []}
    else:
        req = requests.get('http://140.96.178.6:8080/' + querytype + querytime)
        jdic = json.loads(req.text)
    jarr = jdic['SectionData']
    my_dic = {item['SectionId']: item for item in jarr}

    # merge speed information into SensorMap data
    # if no sped information for the sensor, then assign -1
    for item in data:
        if not item['SectionId'] in my_dic:
            item['AvgSpd'] = -1
        else:
            item['AvgSpd'] = my_dic[item['SectionId']]['AvgSpd']

    for item in data:
        sid = item['SectionId']
        if sid in my_dic:
          my_dic[sid]['HighWay'] = item['HighWay']

    cmap = [[-1 for _ in range(dy)] for _ in range(dx)]
    for i in xrange(0, dx):
        for j in xrange(0, dy):
            cell = gridmap[i][j]
            if len(cell) is 0:
                continue
            sum = 0
            count = 0
            for item in cell:
                # my_dic[item][1] to get velocity of item
                if item in my_dic.keys() and my_dic[item]['AvgSpd'] != '-1':
                    spd = float(my_dic[item]['AvgSpd'])
                    if gettype == 'real':
                        spd = spd / 3
                    if my_dic[item]['HighWay'] == '1':
                        spd = spd * 0.6
                    sum += spd
                    count = count + 1
            if count is not 0:
                v = float(sum) / count
                cmap[i][j] = v

    return render(request, 'jampredict/dymap.html',
                  {'data': json.dumps(data), 'cmap': json.dumps(cmap),
                   'time': gettime, 'type':gettype,  'type_ch': querytype_ch,
                   'viewinfo': json.dumps(viewinfo)})
