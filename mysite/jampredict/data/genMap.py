import csv
import json
import pdb

from math import floor

dx = 40
dy = 40

with open("SensorMap.json") as f:

    my_list = json.loads(f.read())
#    my_dic = {}
#    for row in my_list:
#        my_dic[row[0]] = row[1:]

# list2d = [x[:] for x in [[[]]*20]*20]
list2d = []
for i in xrange(0, dx):
    temp = []
    for j in xrange(0, dy):
        temp.append([])
    list2d.append(temp)

gridh = 0.0035
gridw = 0.004
orilat = 24.988391
orilon = 121.473983

for row in my_list:
    clon = float(row['lon'])
    clat = float(row['lat'])
    # latlon = utm.to_latlon(clat, clon, 51, 'R')
    # print str(clat) + "," + str(clon)
    x = (clat - orilat)/gridh
    x = int(floor(x))
    if x < 0:
        x = 0
    if x >= dx:
        x = dx-1
    y = (clon - orilon)/gridw
    y = int(floor(y))

    if y < 0:
        y = 0
    if y >= dy:
        y = dy-1
    list2d[x][y].append(row['SectionId'])
with open('map.json', 'w') as of:
    of.write(json.dumps(list2d))

