import requests
import pdb
import json

req = requests.get('http://140.96.178.6:8080/getSectionInfo')
jd = json.loads(req.text)
data = jd['SectionData']
outArr = []
for item in data:
    sx = float(item['StartWgsX'])
    sy = float(item['StartWgsY'])
    ex = float(item['EndWgsX'])
    ey = float(item['EndWgsY'])
    lat = (sy + ey)/2
    lon = (sx + ex)/2

    tt = {'SectionId': item['SectionId'], 'lat':str(lat), 'lon': str(lon), 'HighWay': str(item['HighWay']) }
    outArr.append(tt)


with open('SensorMap.json', 'w') as of:
    of.write(json.dumps(outArr))

