from django.conf.urls import patterns, include, url
from django.contrib import admin
from jampredict import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    url(r'^dymap$', 'jampredict.views.dymap', name='dymap'),
    url(r'^$', 'jampredict.views.index', name='home'),
)
